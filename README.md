# EEE3088F Design Project

# Motor Driver project

The Motor Driver microHAT is designed for a Raspberry Pi board that adds the ability of driving and controlling two DC motors, while tracking the position of its motors by using a Hall-effect Sensor. This daughter board can be used inside a small toy car, as the DC motors can be attached to its front wheels. This will allow for controlling the speed of the wheels as well as the direction the car would move (reverse and forward). Moreover, this HAT can be attached in a mini-3D printer system where the position of stepped moving parts can be calibrated. The motor driver microHat can furthermore be implemented in door locking systems, the latches can be attached to motors such that it can lock or unlock a door upon sensing requirements being met.

# Use cases 

User role/Scenario1 - Toy Car
●       R1.1:  The Motor IC must support two DC motors used to drive the wheels forward and backwards to allow the toy car to move around.
●       R1.2: The Motor IC needs to deliver variable power to the DC motors which drive the wheels of the toy car to obtain different speeds
●       R1.3: A sensor in the HAT needs to be able to output a signal when the motor-orientation is off,  to the Pi-board, in order to obtain an audio output. 
User role/Scenario2 - 3D Printer
●       R2.1: The Motor IC must support DC motors used to drive the printer along the x,y and z-axis.
●       R2.2: The DC motors need to deliver variable power to the extruder of the 3D printer to move the Hot End at different speeds.
●       R2.3: A sensor needs to be able to determine the position of each motor thus providing feedback as to where the Hot End is positioned.
User role/Scenario3 - Locking mechanism
●       R3.1: The Motor IC must support the DC motors to roll the latch over from the unlocked position to locked position
●       R3.2: The DC motors need to deliver power to roll the latch to and from the locked position
●       R3.3: A sensor needs to be able to know whether it is in the locked position or unlocked position as well as know when to latch and unlatch the door
